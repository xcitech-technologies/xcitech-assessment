import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { AuthService } from './../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) { }

  onClickNavigateDashboard(): void {
    this.router.navigate(['dashboard'])
  }
  onClickNavigateAdmin(): void {
    this.router.navigate(['admin-list']);
  }


  ngOnInit() {
  }
  logout() {
    this.authService.doLogout()
  }
}
